#include <stdlib.h>
#include <stdio.h>

char s0[] = "\0";
char s1[] = "Line1 text\0";
char s2[] = "line2 text\0";
char s3[] = "line3 text\0";
char s4[] = "line4 text\0";


char* screen1[] = {s0};
#define screen1Len (sizeof(screen1)/sizeof(&screen1))
char* screen2[] = {s0,s1,s2,s3,s4};
#define screen2Len (sizeof(screen2)/sizeof(&screen2))
char* screen3[] = {s0,s1};
#define screen3Len (sizeof(screen3)/sizeof(&screen3))

char** screens[] = {screen1,screen2,screen3};
int screensLen[] = {screen1Len,screen2Len,screen3Len};

void drawScreen(char**,unsigned,int);
void next(char**,unsigned*,int);
void prev(char**,unsigned*,int);
float min(float a, float b){return (a < b ? a : b);}

int main(){
	char key=0xff;
	int i = 0;
	unsigned* offset;
	offset = malloc(sizeof(unsigned));
	*offset = 0;
	drawScreen(screens[1],0,screensLen[1]);
	while(1){
		key = getchar();
		switch (key) {
			case 'n' : {
				next(screens[1],offset,screensLen[1]);
			}; break;
			case 'p' : {
				prev(screens[1],offset,screensLen[1]);
			}; break;
		}
	}
	return 1;
}

void drawScreen(char** s,unsigned yo,int scrLen){
	int i = 0;
	system("clear");
	for(i; i<(unsigned)min(scrLen, 2); i++){
		printf("%d ",i);
		printf(i==1?">":" ");
		printf("%s\n",s[i+yo]);
	}
}

void next(char** s,unsigned* menuOffset,int scrLen){
		*menuOffset = ((*menuOffset)+2==scrLen)?(*menuOffset):(((*menuOffset)+1));
		drawScreen(s,*menuOffset,scrLen);
}

void prev(char** s, unsigned* menuOffset,int scrLen){
		*menuOffset = (*menuOffset==0)?(*menuOffset):((*menuOffset)-1);
			drawScreen(s,*menuOffset,scrLen);
}
