#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int a;
	int b;
	int (*ptFunction) (int, int); 
} fraction;

int DoIt (int x, int y){ printf("DoIt\n"); return x+y;}
int DoIt2 (int x, int y){ printf("DoIt2\n"); return y-x;}

int main(){
	fraction *f;
	f = malloc(sizeof(fraction)*2);

	f[0].a = 5;
	f[0].b = 6;
	f[0].ptFunction = &DoIt;
	f[1].a = 10;
	f[1].b = 11;
	f[1].ptFunction = &DoIt2;

	printf("f[1].a: %d, f[1].b: %d \n",f[1].a,f[1].b);
	printf("call 0 : %d\n",f[0].ptFunction(f[0].a,f[0].b));
	printf("call 1 : %d\n",f[1].ptFunction(f[1].a,f[1].b));

	free(f);

	return 1;
}
