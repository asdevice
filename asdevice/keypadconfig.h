#ifndef KEYPADCONF_H
#define KEYPADCONF_H

#ifndef KEYPAD_H
#include "keypad.h"
#endif

#define KEYPAD_4x4
//#define KEYPAD_3x4
#define debounceTime 20
//port settings
//0-3 - colls -OUT
//4-7 - rows - IN
#define INIT_PORT DDRD = 0x0f
#define INIT_PULLUP PORTD = 0xff
#define PORT_IN PIND
#define PORT_OUT PORTD
#define keyPadCols 4
#define keyPadRows 4

#endif
