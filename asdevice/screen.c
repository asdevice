#include "screen.h"

// s - stands for array of pointers to array of screen texts
// xo - stands for x-Offset (how much spaces in front of line)
// yo - stands for y-Offset (which line first)
void drawScreen(PGM_P* s, unsigned xo, unsigned yo, int scrSize){
	lcdClear();
	rprintfInit(lcdDataWrite);
	for(int i = 0; i<(unsigned)fmin(((sizeof(s)/sizeof(&s))), LCD_LINES); i++){
		lcdGotoXY(i,xo);
		rprintfProgStr(s[i+yo]);
		if(i == 1){rprintfChar(SELCHAR);}
	}
}

void prev(PGM_P* s, DeviceState* d, int scrSize){
	(*d).menuOffset = ((*d).menuOffset==0)?((*d).menuOffset):((*d).menuOffset-1);
	drawScreen(s,2,(*d).menuOffset, scrSize);
}

void next(PGM_P* s, DeviceState* d, int scrSize){
	(*d).menuOffset = ((*d).menuOffset==scrSize)?((*d).menuOffset):((*d).menuOffset+1);
	drawScreen(s,2,(*d).menuOffset,scrSize);
}