#ifndef SCREEN_H
#define SCREEN_H

#ifndef GLOBAL_H
#include "global.h"
#endif
#ifndef LCDCONF_H
#include "lcdconf.h"
#endif
#ifndef LCD_H
#include "lcd.h"
#endif
#ifndef RPRINTF_H
#include "rprintf.h"
#endif
#ifndef __PGMSPACE_H_
#include <avr/pgmspace.h>
#endif
#ifndef NAN
#include <math.h>
#endif
#ifndef DEVICE_TYPES_H
#include "devicetypes.h"
#endif

#ifndef SELCHAR
#define SELCHAR 0x3e	// selchar is '*'
#endif

// s - stands for array of pointers to array of screen texts
// xo - stands for x-Offset (how much spaces in front of line)
// yo - stands for y-Offset (which line first)
void drawScreen(PGM_P*,unsigned,unsigned,int);
void next(PGM_P*,DeviceState*,int);
void prev(PGM_P*,DeviceState*,int);

#endif