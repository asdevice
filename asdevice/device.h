#ifndef DEVICE_H
#define DEVICE_H

#include <stdlib.h>
#include <avr/io.h>

#ifndef GLOBAL_H
#include "global.h"
#endif
#ifndef __PGMSPACE_H_
#include <avr/pgmspace.h>
#endif
#ifndef NAN
#include <math.h>
#endif
#ifndef LCD_H
#include "lcd.h"
#endif
#include "lcdconf.h"
#ifndef KEYPADCONF_H
#include "keypadconfig.h"
#endif
#ifndef SCREEN_H
#include "screen.h"
#endif
#ifndef DEVICE_TYPES_H
#include "devicetypes.h"
#endif
#ifndef RPRINTF_SIMPLE
#include "rprintf.h"
#endif

#endif
