#ifndef KEYPAD_H
#define KEYPAD_H
#include <avr/pgmspace.h>
#ifndef GLOBAL_H
	#include "global.h"
#endif
#ifndef KEYPADCONF_H
	#include "keypadconfig.h"
#endif
#ifndef _UTIL_DELAY_H_
	#include <util/delay.h>
#endif

#ifdef KEYPAD_4x4
#define kpcolls 4
#define kprows 4

unsigned char keyboard[4][4] PROGMEM = 
{
	{'1','2','3','A'},
	{'4','5','6','B'},
	{'7','8','9','C'},
	{'*','0','#','D'}
};
#endif
#ifdef KEYPAD_3x4
#define kpcolls 3
#define kprows 4
unsigned char keyboard[4][3] PROGMEM = 
{
	{'1','2','3'},
	{'4','5','6'},
	{'7','8','9'},
	{'*','0','#'}
};
#endif

typedef struct{
	unsigned pressed : 1;
	unsigned holded : 1;
	unsigned released : 1;
	char keyChar;
} KeyPress;

void keyPadInit(void);
char getKey(void);
#endif
