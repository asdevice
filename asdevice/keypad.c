#include "keypad.h"

void keyPadInit(){
	INIT_PORT;
	INIT_PULLUP;
};

char getKey(){
	unsigned char upperNibble = 0xff;
	unsigned char keyCode = 0xff;

	for(unsigned i=0; i<4; i++){
		_delay_ms(1);
		PORT_OUT = ~(0x01 << i);
		_delay_ms(1); //delay for port o/p settling
		upperNibble = PORT_IN | 0x0f;

	if (upperNibble != 0xff){
		_delay_ms(debounceTime); //key debouncing delay
		upperNibble = PORT_IN | 0x0f;
		if(upperNibble == 0xff) goto OUT;

		keyCode = (upperNibble & 0xf0) | (0x0f & ~(0x01 << i));

		while (upperNibble != 0xff)
			upperNibble = PORT_IN | 0x0f;

			_delay_ms(debounceTime); //key debouncing delay

			switch (keyCode){
				case (0xee): keyCode = '0';	break;
				case (0xed): keyCode = '1';	break;
				case (0xeb): keyCode = '2';	break;
				case (0xe7): keyCode = '3';	break;
				case (0xde): keyCode = '4';	break;
				case (0xdd): keyCode = '5';	break;
				case (0xdb): keyCode = '6';	break;
				case (0xd7): keyCode = '7';	break;
				case (0xbe): keyCode = '8';	break;
				case (0xbd): keyCode = '9';	break;
				case (0xbb): keyCode = 'A';	break;
				case (0xb7): keyCode = 'B';	break;
				case (0x7e): keyCode = 'C';	break;
				case (0x7d): keyCode = 'D';	break;
				case (0x7b): keyCode = '*';	break;
				case (0x77): keyCode = '#';	break;
				default : keyCode = 'X';
			}
			OUT:;
		}
	}
return keyCode;
};
