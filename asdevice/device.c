/**
 ************************************************************************************************ 
 ** Program pre zariadenie emulujuce nastrazny system pozostavajuce z : 
 **  - 2x majak
 **  - 1x sirena
 **  - 1x LCD
 **  - 1x keypad 4x4
 ** Umoznuje hrat scenar pre 2 timy, kazdy z timov sa snazi o zapnutie resp. vypnutie zariadenia.
 ************************************************************************************************ 
**/
#include "device.h"

// menu strings
//---------------------0123456789ABCDEF----
char s000[] PROGMEM = "\0";
char s001[] PROGMEM = "Made by OP-F\0";
char s002[] PROGMEM = "Start game\0";
char s003[] PROGMEM = "Game Setup\0";
char s004[] PROGMEM = "Device state\0";
char s005[] PROGMEM = "Act/Def codes:\0";
char s006[] PROGMEM = "Game begin in:\0";
char s007[] PROGMEM = "Enter CODE:\0";
char s008[] PROGMEM = "ACTIVE IN:\0";
char s009[] PROGMEM = "DEFUSED IN:\0";
char s010[] PROGMEM = "LAST ACTION\0";
char s011[] PROGMEM = "!CANCELLED!\0";
char s012[] PROGMEM = "Device\0";
char s013[] PROGMEM = "DEFUSED!\0";
char s014[] PROGMEM = "ACTIVATED!\0";
char s015[] PROGMEM = "!! GAME OVER !!\0";
char s016[] PROGMEM = "!! BLUE WINS !!\0";
char s017[] PROGMEM = "!! RED WINS  !!\0";


//set screens
PGM_P welcomeScreen[] PROGMEM = {s000,s001};
#define welcomeScreenSize (sizeof(welcomeScreen)/sizeof(&welcomeScreen))
PGM_P mainScreen[] PROGMEM = {s002,s003,s004};
#define mainScreenSize (sizeof(mainScreen)/sizeof(&mainScreen))
PGM_P codeScreen[] PROGMEM = {s005};
#define codeScreenSize (sizeof(codeScreen)/sizeof(&codeScreen))


PGM_P* screens[] PROGMEM = {welcomeScreen,mainScreen,codeScreen};
int screensSizes[] = {welcomeScreenSize,mainScreenSize,codeScreenSize};

void welcomeAction(DeviceState*, PGM_P*, int scrSize);
void mainAction(DeviceState*, PGM_P*, int scrSize);
void startGame(DeviceState*, PGM_P*, int scrSize);
void setupDevice(DeviceState*, PGM_P*, int scrSize);
void showCodes(DeviceState*, PGM_P*, int scrSize);

void delay_us(unsigned short time_us);
void generateCodes(DeviceState*);
long getUniqueRandom(DeviceState* d);

int main(void){
	// LCD init and printf to LCD
	lcdInit();

	// set initial device state and menu
	DeviceState* device = malloc(sizeof(DeviceState));

	(*device).stateChanged = 1;
	(*device).screenChanged = 0;
	(*device).stateId = 0;
	(*device).menuOffset = 0;

	//set function array
	StateAction* actions = malloc(sizeof(StateAction)*10);
	actions[0].action = welcomeAction;
	actions[1].action = mainAction;
	actions[3].action = startGame;


	while(1){
		actions[(*device).stateId].action(device,screens[(*device).stateId],screensSizes[(*device).stateId]);
	}

	return 0;
}

void welcomeAction(DeviceState* d, PGM_P* s, int scrSize){
	char key = getKey();
	
	if((*d).stateChanged){
		(*d).menuOffset = 0;
		drawScreen(s,2,(*d).menuOffset,scrSize);
		(*d).stateChanged = 0;
	} else {
		if(key != 0xff){
			(*d).stateId = 1;
			(*d).stateChanged = 1;
			(*d).menuOffset = 0;
		}
	}
}

void mainAction(DeviceState* d, PGM_P* s, int scrSize){
	char key = getKey();
	
	if((*d).stateChanged){
		lcdClear(); lcdGotoXY(0,0);
		(*d).menuOffset = 0;
		drawScreen(s,2,(*d).menuOffset,scrSize);
		(*d).stateChanged = 0;
	} else {
		if(key != 0xff){
			switch (key) {
				case '*': {
					next(s,d,scrSize);
				}; break;
				case '#' : {
					prev(s,d,scrSize);
				}; break;
				case '0' : {
					if((*d).menuOffset == 1) {
						(*d).stateChanged = 1;
						(*d).stateId = 2;
					}
				}; break;
			}
		}
	}
}

void startGame(DeviceState* d, PGM_P* s, int scrSize){
	char key = getKey();
	
	if((*d).stateChanged){
		lcdClear(); lcdGotoXY(0,0);
		(*d).menuOffset = 0;
		drawScreen(s,2,(*d).menuOffset,scrSize);
		(*d).stateChanged = 0;
	} else {
		switch (key) {
			case '*': {
				next(s,d,scrSize);
			}; break;
			case '#' : {
				prev(s,d,scrSize);
			}; break;
		}
	}
}

void delay_us(unsigned short time_us){
	unsigned short delay_loops;
	register unsigned short i;
	
	delay_loops = (time_us+3)/5*CYCLES_PER_US; // +3 for rounding up (dirty) 
	// one loop takes 5 cpu cycles 
	for (i=0; i < delay_loops; i++) {};
}

void generateCodes(DeviceState* d){
	for(int i=0; i<5; i++){(*d).aCodes[i] = getUniqueRandom(d);}
	for(int i=0; i<5; i++){(*d).dCodes[i] = getUniqueRandom(d);}
}

long getUniqueRandom(DeviceState* d){
	int run = 1;
	long rnd = 0;
	
	while(run){
		run = 0;
		rnd = random()%100000;
		for(int i=0; i<5; i++){ if(rnd == (*d).aCodes[i]){run = 1;}}
		for(int i=0; i<5; i++){ if(rnd == (*d).dCodes[i]){run = 1;}}
	}
	return rnd;
}