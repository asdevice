#ifndef DEVICE_TYPES_H
#define DEVICE_TYPES_H

typedef struct{
	unsigned screenChanged:1;
	unsigned stateChanged:1;
	unsigned lcdBckLight:1;
	unsigned redFlash:1;
	unsigned greenFlash:1;
	unsigned int stateId;
	unsigned long holdedFor;
	unsigned menuOffset;
	unsigned long aCodes[5];
	unsigned long dCodes[5];
} DeviceState;

typedef struct{
	void (*action)(DeviceState*, PGM_P*,int);
} StateAction;

typedef struct{
	unsigned char** screenLines;
	unsigned* displayedLines;
	unsigned selectedLine;
} Screen;

#endif