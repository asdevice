#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct{
	long aCodes[5];
	long dCodes[5];
} Data;

long getUniqueRandom(Data* d){
	int run = 1;
	long rnd = 0;
			
	while(run){
		run = 0;
		srand(time(NULL));
		rnd = rand()%100000;
		for(int i=0; i<5; i++){ if(rnd == (*d).aCodes[i]){run = 1;}}
		for(int i=0; i<5; i++){ if(rnd == (*d).dCodes[i]){run = 1;}}
	}
	return rnd;
}

int main(void){
	Data* d = malloc(sizeof(Data));

	for(int i=0; i<5; i++){(*d).aCodes[i] = getUniqueRandom(d);}
	for(int i=0; i<5; i++){(*d).dCodes[i] = getUniqueRandom(d);}

	for(int i=0; i<5; i++){ printf("%05ld\n",(*d).aCodes[i]);}
	for(int i=0; i<5; i++){ printf("%05ld\n",(*d).dCodes[i]);}
	
	return 0;
}
